<?php

declare(strict_types=1);

namespace TYPO3SoulMate\AfeyFaq\Controller;


use Psr\Http\Message\ResponseInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3SoulMate\AfeyFaq\Domain\Repository\FaqRepository;

/**
 * This file is part of the "FAQ" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2024 Aleksandra Fey <aleksandra.fey@outlook.de>
 */

/**
 * FaqController
 */
class FaqController extends ActionController
{

    /**
     * faqRepository
     *
     * @var FaqRepository|null
     */
    protected ?FaqRepository $faqRepository = null;

    /**
     * @param FaqRepository $faqRepository
     */
    public function injectFaqRepository(FaqRepository $faqRepository)
    {
        $this->faqRepository = $faqRepository;
    }

    /**
     * action list
     *
     * @return ResponseInterface
     */
    public function listAction(): ResponseInterface
    {
        $faqsLimit = $this->settings['limit_records'] ?? 0;

        if ($this->settings['enable_faqs_selection']) {
            $faqs = $this->getFaqsSelection($this->settings['faqs_selection']);
        } elseif ($this->settings['enable_sorting']) {
            $faqs = $this->faqRepository->findAllOrdered('question', $this->settings['sort_order']);
        } else {
            $faqs = $this->faqRepository->findAll();
        }

        $this->view->assignMultiple([
            'faqs' => $faqs,
            'faqs_limit' => $faqsLimit
        ]);

        return $this->htmlResponse();
    }

    /**
     * Retrieve selected FAQ records based on provided UIDs.
     *
     * @param string $faqsSelection A comma-separated string of FAQ UIDs.
     * @return array|null An array containing selected FAQ records, or null if no selection is provided.
     */
    protected function getFaqsSelection(string $faqsSelection): ?array
    {
        $faqsUids = GeneralUtility::trimExplode(',', $faqsSelection);

        $faqs = [];
        foreach ($faqsUids as $faqUid) {
            $faq = $this->faqRepository->findByUid((int)$faqUid);
            if ($faq !== null) {
                $faqs[] = $faq;
            }
        }

        return !empty($faqs) ? $faqs : null;
    }
}
