<?php

declare(strict_types=1);

namespace TYPO3SoulMate\AfeyFaq\Domain\Model;


use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;

/**
 * This file is part of the "FAQ" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2024 Aleksandra Fey <aleksandra.fey@outlook.de>
 */

/**
 * FAQ
 */
class Faq extends AbstractEntity
{

    /**
     * question
     *
     * @var string
     */
    protected string $question = '';

    /**
     * answer
     *
     * @var string
     */
    protected string $answer = '';

    /**
     * Returns the question
     *
     * @return string
     */
    public function getQuestion(): string
    {
        return $this->question;
    }

    /**
     * Sets the question
     *
     * @param string $question
     * @return void
     */
    public function setQuestion(string $question)
    {
        $this->question = $question;
    }

    /**
     * Returns the answer
     *
     * @return string
     */
    public function getAnswer(): string
    {
        return $this->answer;
    }

    /**
     * Sets the answer
     *
     * @param string $answer
     * @return void
     */
    public function setAnswer(string $answer)
    {
        $this->answer = $answer;
    }
}
