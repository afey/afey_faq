<?php

declare(strict_types=1);

namespace TYPO3SoulMate\AfeyFaq\Domain\Repository;


use TYPO3\CMS\Extbase\Persistence\QueryInterface;
use TYPO3\CMS\Extbase\Persistence\QueryResultInterface;
use TYPO3\CMS\Extbase\Persistence\Repository;

/**
 * This file is part of the "FAQ" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * (c) 2024 Aleksandra Fey <aleksandra.fey@outlook.de>
 */

/**
 * The repository for Faqs
 */
class FaqRepository extends Repository
{
    /**
     * Find all records ordered.
     *
     * @param string $propertyName The name of the property to sort by.
     * @param string $order The sorting order ('ASC' for ascending, 'DESC' for descending).
     * @return QueryResultInterface|array
     */
    public function findAllOrdered(string $propertyName, string $order = 'ASC'): QueryResultInterface
    {
        $query = $this->createQuery();
        $order = (strtoupper($order) === 'DESC') ? QueryInterface::ORDER_DESCENDING : QueryInterface::ORDER_ASCENDING;
        $query->setOrderings([$propertyName => $order]);
        return $query->execute();
    }
}
