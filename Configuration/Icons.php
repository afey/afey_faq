<?php

use TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider;

return [
    'afey_faq-plugin-faq' => [
        'provider' => SvgIconProvider::class,
        'source' => 'EXT:afey_faq/Resources/Public/Icons/user_plugin_faq.svg'
    ],
];
