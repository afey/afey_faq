<?php

use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Extbase\Utility\ExtensionUtility;

defined('TYPO3') || die();

ExtensionUtility::registerPlugin('AfeyFaq','Faq','FAQ');

$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist']['afeyfaq_faq'] = 'recursive, pages';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist']['afeyfaq_faq'] = 'pi_flexform';
ExtensionManagementUtility::addPiFlexFormValue(
    'afeyfaq_faq',
    'FILE:EXT:afey_faq/Configuration/FlexForms/FaqSettings.xml'
);
