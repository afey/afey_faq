# Test: „FAQ“ plugin reqirements:

 - Es sollen im Backend Datensätze vom Typ „FAQ“ angelegt werden können.
 - Ein FAQ Datensatz besteht aus einer Frage mit einer Antwort.
 - Es soll ein Plugin geben mit welchem die Datensätze ausgegeben werden können.
 - Es soll im Plugin möglich sein entweder alle Datensätze auszugeben oder einzelne Datensätze auszuwählen.
 - Werden einzelne Datensätze ausgewählt sollen diese im Frontend gleich der Reihenfolge im Plugin ausgegeben werden.
 - Werden alle Datensätze ausgegeben soll die Möglichkeit bestehen im Plugin anzugeben ob diese alphabetisch aufsteigend oder absteigend sortiert werden sollen
 - Zusätzlich soll die Anzahl der auszugebenden Datensätze im Plugin limitiert werden können.

_Developed in TYPO3 version 11.5 by Aleksandra Fey._

## Installation

Download the extension and put it in your packages' folder.
The extension can be installed via composer: `ddev composer req typo3soulmate/afey-faq`. Use `ddev`, if you use ddev.
For legacy installation you can copy the extension in your ext folder and installed via extension manager.

Run database structure compare for table `tx_afeyfaq_domain_model_faq`.

## Setup TypoScript Template via Backend

 - Navigate in the TYPO3 Backend module "Template" on the site root page.
 - Choose "Info/Modify" and "Edit the whole template record".
 - Under the "Includes" tab, under "Include static (from extensions)", under "Available Items" choose the faq TypoScript.
 - Make sure is placed before the site package extension, because it could be overrides there if necessary.
 - Flush TYPO3 backend cache.

### OR Setup TypoScript Template via import in code

- In your TypoScript site package extension you can import the main TypoScript setup and constants files for this plugin.
  `@import 'EXT:afey_faq/Configuration/TypoScript/setup.typoscript'`
- Flush TYPO3 backend cache.

# Setup: page, plugin and storage
- Place the FAQ - Frequently Asked Questions Frontend Plugin on a page. It could be found under "New Content Elements" Wizard under Tab "Plugins".
- You can configure the plugin by enabling/disabling the 'Enable individual data records selection' checkbox.
  - If enabled: Editors can selectively choose individual faqs records for the output.
  - If disabled:
    - All data records are outputted without the option for selective choosing.
    - Sorting order can be enabled.
    - Sort order can be defined (ascending or descending).
- To facilitate the organization of records across pages, a fixed storage location is not defined. Editors can manually organize FAQ records via the backend. Editors can create a storage system folder where FAQ records can be stored.
