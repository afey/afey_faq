<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'FAQ',
    'description' => 'Frequently Asked Questions Frontend Plugin',
    'category' => 'plugin',
    'author' => 'Aleksandra Fey',
    'author_email' => 'aleksandra.fey@outlook.de',
    'state' => 'alpha',
    'clearCacheOnLoad' => 0,
    'version' => '1.0.0',
    'constraints' => [
        'depends' => [
            'typo3' => '11.5.0-11.5.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
